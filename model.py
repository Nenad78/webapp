from wtforms import Form, FloatField, validators


class InputForm(Form):
    k = FloatField(
        label='stepen slobode Erlangove raspodele (ceo pozitivan broj)', default=1,
        validators=[validators.InputRequired()])
    l = FloatField(
        label='frekvencija između 2 uzastopna nailaska u sistem (1/s)', default=0.2,
        validators=[validators.InputRequired()])
    ordinata = FloatField(
        label='domen slučajne promenljive x', default=100,
        validators=[validators.InputRequired()])
