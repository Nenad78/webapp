from numpy import exp, cos, linspace
import matplotlib.pyplot as plt
import os, time, glob
from math import factorial


def erlangova_raspodela(x, k, l):
    return l ** k * x ** (k - 1) * exp(-l * x) / factorial(k - 1)


def compute(k, l, ordinata, resolution=500):
    x = linspace(0, ordinata, resolution + 1)
    f = erlangova_raspodela(x, k, l)
    plt.figure()
    plt.plot(x, f)
    plt.title('k=%g, l=%g' % (k, l))
    if not os.path.isdir('static'):
        os.mkdir('static')
    else:
        for filename in glob.glob(os.path.join('static', '*.png')):
            os.remove(filename)
    plotfile = os.path.join('static', str(time.time()) + '.png')
    plt.savefig(plotfile)
    return plotfile


if __name__ == '__main__':
    print(compute(1, 0.2, 100))
